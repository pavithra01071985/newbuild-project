export const authToken = () => {
  let tokenitem = document.cookie.split(";").find((i) => {
    return i.split("=")[0].trim() === "access_token";
  });
  return tokenitem ? tokenitem.split("=")[1] : "";
};
