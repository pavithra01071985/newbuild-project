import buildUrl from "build-url";
import React, { Component } from "react";
import Avatar from "./avatar";

export default class Header extends Component {
  render() {
    const serverUrl = process.env.REACT_APP_GITLAB_URL;
    return (
      <nav className="App-header navbar navbar-default navbar-static-top">
        <div className="container">
          <div className="collapse navbar-collapse">
            <ul className="nav navbar-nav">
              <li>
                <a
                  href={buildUrl(serverUrl, {
                    path: this.props.project,
                  })}
                >
                  <span className="glyphicon glyphicon-home" />
                </a>
              </li>
            </ul>
            <ul className="nav navbar-nav navbar-right">
              <li>
                <Avatar />
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}
